# Terrain generation with quantum computation

Written using the Python-embedded quantum programming language Ket.

* https://quantum-ket.gitlab.io

To run you need Ket interpreter and KBW simulator.

## install

```shell
$ sudo snap install kbw --edge
$ sudo snap install ket --edge
```
> *Linux only.*

## Run

Start the KBW server in a terminal

```shell
$ kbw
```
> ```
> Ket Biswise Simulator server
> ============================
> 
> Setting up:
> 	Seed		2029
> 	Plugin PATH	/snap/kbw/100/lib/python3.8/site-packages/kbw
> 	Bind		127.0.1.1:4242
> 
> Use Ctrl+c to stop the server
> 
> Waiting connection...
> ```

and in another terminal run 

```shell
$ ./line.ket
```

> ```
> [2, 3, 2, 3, 3, 3, 3, 3, 2, 1, 0, 0, 1, 0, 1]
> ```

```shell
$ ./matrix.ket
```

> ```
> [2, 3, 2, 1]
> [3, 3, 3, 2]
> [3, 2, 2, 3]
> [2, 3, 3, 3]
> ```




 